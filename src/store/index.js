import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    goodsItem: [], //所有的分类
    oneNav: [], //左侧的一级分类
    twoNav: [], //右侧的二级分类
    showList: [], //显示数据 
    cartItem: JSON.parse(localStorage.getItem("cartItems")) || [], //保存到本地的数据
    allChecked: false,
    UnitPrice: 0, //单价
    total: 0, //总价
  },
  mutations: {
    // 所有的数据
    GOODS_DATA(state, items) {
      state.goodsItem = items;
      state.oneNav = items.slider;
      state.twoNav = items.list;
      state.showList = items.list;
    },
    // 点击切换
    SELECTINDEX(state, item) {
      state.showList = [];
      if (item.title == "All") {
        state.showList = state.twoNav;
      } else {
        state.twoNav.forEach((element) => {
          if (element.salePrice > item.low && element.salePrice <= item.hight) {
            state.showList.push(element);
          }
        })
      }
    },
    // 刷新页面的数据
    GET_XIN_DATA(state, items) {
      state.twoNav = [...state.twoNav, ...items];
      state.showList = [...state.showList, ...items];
    },
    // 添加购物车
    ADDCART_DATA(state, item) {
      let flag = false;
      state.cartItem.map(ele => {
        console.log(ele)
        if (ele.item._id == item._id) {
          flag = true;
          ele.count++
        }
      })
      if (!flag) {
        state.cartItem.push({
          item,
          checked: false,
          count: 1
        })
      }
      state.total = 0;
      state.cartItem.forEach(ele => {
        if (ele.checked) {
          state.total += ele.item.salePrice * ele.count;
        }
      })
      localStorage.setItem('cartItems', JSON.stringify(state.cartItem))
    },
    // 点击商品前面的商品的复选框
    SELECT_ITEM(state, item) {
      item.checked = !item.checked;
      var temp = state.cartItem.every(ele => {
        return ele.checked == true;
      });
      state.allChecked = temp;
      state.total = 0;
      state.cartItem.forEach(ele => {
        if (ele.checked) {
          state.total += ele.item.salePrice * ele.count;
        }
      })
      localStorage.setItem('cartItems', JSON.stringify(state.cartItem))
    },
    // 点击底部的全选按钮
    ALLSELECT(state) {
      state.allChecked = !state.allChecked;
      state.cartItem.forEach(ele => {
        ele.checked = state.allChecked;
      });
      state.total = 0;
      state.cartItem.forEach(ele => {
        if (ele.checked) {
          state.total += ele.item.salePrice * ele.count;
        }
      })
      localStorage.setItem('cartItems', JSON.stringify(state.cartItem))
    },
    // 减商品
    SUB_DATA(state, item) {
      if (item.count > 1) {
        item.count--;
      }
      state.total = 0;
      state.cartItem.forEach(ele => {
        if (ele.checked) {
          state.total += ele.item.salePrice * ele.count;
        }
      })
      localStorage.setItem('cartItems', JSON.stringify(state.cartItem))
    },
    // 加商品
    ADD_DATA(state, item) {
      item.count++;
      state.total = 0;
      state.cartItem.forEach(ele => {
        if (ele.checked) {
          state.total += ele.item.salePrice * ele.count;
        }
      })
      localStorage.setItem('cartItems', JSON.stringify(state.cartItem))
    },
    // 删除数据
    DELETE(state, index) {
      state.cartItem.splice(index, 1);
      state.total = 0;
      state.cartItem.forEach(ele => {
        if (ele.checked) {
          state.total += ele.item.salePrice * ele.count;
        }
      })
      localStorage.setItem('cartItems', JSON.stringify(state.cartItem))
    }
  },
  actions: {
    // 所有的分类
    GOODS_DATA(context) {
      axios.get("/data.json").then(res => {
        // console.log(res.data.result);
        context.commit('GOODS_DATA', res.data.result);
      })
    },
    // 点击切换
    SELECTINDEX(context, item) {
      context.commit('SELECTINDEX', item)
    },
    // 刷新页面的数据
    GET_XIN_DATA(contxet, page) {
      if (page > 3) {
        return
      }
      axios.get(`/data${page}.json`).then(res => {
        setTimeout(() => {
          contxet.commit("GET_XIN_DATA", res.data.result.list)
        }, 3000);
      })
    },
    // 添加购物车
    ADDCART_DATA(context, item) {
      context.commit('ADDCART_DATA', item)
    },
    // 点击商品前面的商品的复选框
    SELECT_ITEM(context, item) {
      context.commit('SELECT_ITEM', item)
    },
    // 点击底部的全选按钮
    ALLSELECT(context) {
      context.commit('ALLSELECT')
    },
    // 减商品
    SUB_DATA(context, item) {
      context.commit('SUB_DATA', item)
    },
    // 加商品
    ADD_DATA(context, item) {
      context.commit('ADD_DATA', item)
    },
    // 删除数据
    DELETE(context, index) {
      context.commit('DELETE', index)
    }
  },
  modules: {}
})